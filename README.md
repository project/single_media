CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module is very similar to [Single Image]
(https://www.drupal.org/project/single_image_formatter) module, but it is 
intended only for media and more flexible, as it allows you to choose 
the "delta" - the position of the desired image.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/single_media
 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/single_media


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the module as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

After enabling the module, choose the "Media delta" option in the
"Manage display" tab of your entity.


MAINTAINERS
-----------

Current maintainers:

 * Roman Salo
   Drupal: https://www.drupal.org/u/rolki
